package com.example.saregistracio

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var name = findViewById<EditText>(R.id.name)
        var lastName = findViewById<EditText>(R.id.lastName)
        var email = findViewById<EditText>(R.id.email)
        var password = findViewById<EditText>(R.id.password)
        val toast = findViewById<Button>(R.id.button)

        toast.setOnClickListener {
            if (name.text.length > 2 &&  lastName.text.length > 4 && password.text.length > 7 && email.text.contains("@") ){
                Toast.makeText(this, "რეგისტრაცია წარმატებით დასრულდა", Toast.LENGTH_SHORT).show()

            }else Toast.makeText(this, "შეიყვანეთ მონაცემები სწორად", Toast.LENGTH_SHORT).show()
        }
    }
}
